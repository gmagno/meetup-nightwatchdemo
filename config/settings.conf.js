var SELENIUM_CONFIG = {
	start_process: true,
	server_path : "./drivers/selenium-2.53.1/selenium-server-standalone-2.53.1.jar",
	log_path : "./reports/logs",
	host: "localhost",
	port: 4444,
	cli_args : {
      "webdriver.chrome.driver" : "./drivers/chromedriver/chromedriver",
      "webdriver.ie.driver" : "",
      "webdriver.gecko.driver": "./drivers/geckodriver/geckodriver3"
    }
};

var FIREFOX_CONFIG = {
	browserName: "firefox",
	cleanSession: true,
	javascriptEnabled: true,
	acceptSslCerts: true,
	marionette: true
};

var CHROME_CONFIG = {
	browserName: "chrome",
    cleanSession: true,
    javascriptEnabled: true,
    applicationCacheEnabled: true,
    acceptSslCerts: true,
    cssSelectorsEnabled: true,
    chromeOptions: {
          args: ["test-type"]
    }
};

var CHROME_ENV = {
	launch_url:"https://demo.silverstripe.org",
	selenium_port:4444,
	selenium_host:"localhost",
	desiredCapabilities: CHROME_CONFIG
};

var DEFAULT_ENV = {
	launch_url:"https://demo.silverstripe.org",
	selenium_port:4444,
	selenium_host:"localhost",
	desiredCapabilities: FIREFOX_CONFIG
};

var ENVIRONMENTS = {
	default: DEFAULT_ENV,
	chrome: CHROME_ENV,
};

module.exports = {
	src_folders: ['./tests'],
	output_folder : "./reports",
  	globals_path : "data/globals.js",
  	page_objects_path: "./resources/pages",
  	//custom_commands_path: "resources/templates",
  	test_workers: {
    	"enabled": true,
    	"workers": "auto"
  	},
	selenium: SELENIUM_CONFIG,
	test_settings: ENVIRONMENTS
};
