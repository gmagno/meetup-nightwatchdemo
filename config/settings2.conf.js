var SELENIUM_CONFIG = {
	start_process: false,
	server_path : "",
	log_path : "",
	host: "ondemand.saucelabs.com",
	port: 80,
	cli_args : {
      "webdriver.chrome.driver" : "./drivers/chromedriver/chromedriver",
      "webdriver.ie.driver" : "",
      "webdriver.gecko.driver": "./drivers/geckodriver/geckodriver3"
    }
};

var FIREFOX_CONFIG = {
  desiredCapabilities: {
  	browserName: "firefox",
  	javascriptEnabled: true,
  	acceptSslCerts: true,
  	marionette: true,
    platform: "OS X 10.11",
    version: 47
  }
};

var CHROME_CONFIG = {
	  desiredCapabilities: {
      browserName: "chrome",
      cleanSession: true,
      javascriptEnabled: true,
      applicationCacheEnabled: true,
      acceptSslCerts: true,
      cssSelectorsEnabled: true,
      chromeOptions: {
            args: ["test-type"]
      },
      platform: "OS X 10.11",
      version: 47
    }
};

var SAUCELABS_ENV = {
      launch_url:"https://demo.silverstripe.org",
      selenium_host: "ondemand.saucelabs.com",
      selenium_port: 80,
      username: "angeltan000111-100",
      access_key: "82bed838-e26b-497c-9edb-2a68dad3ef4e",
      desiredCapabilities: {
      	javascriptEnabled: true,
      	acceptSslCerts: true
      }
};

var ENVIRONMENTS = {
  default: SAUCELABS_ENV,
  chrome: CHROME_CONFIG,
  firefox: FIREFOX_CONFIG
};

module.exports = {
	src_folders: ['./tests'],
	output_folder : "./reports",
  globals_path : "data/globals.js",
  page_objects_path: "./resources/pages",
  	//custom_commands_path: "resources/templates",
  test_workers: {
    "enabled": false,
    "workers": "auto"
  },
	selenium: SELENIUM_CONFIG,
	test_settings: ENVIRONMENTS
};
