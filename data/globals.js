var HtmlReporter = require('nightwatch-html-reporter');
var reporter = new HtmlReporter( {
    openBrowser: true,
    reportsDirectory: 'reports/',
    reportFilename: 'generatedReport.html',
    hideSuccess: false
});

var data = {
    login : {
        correct_credentials: {
            email: "admin",
            password: "password"
        }
    }
};

module.exports = {
    reporter: reporter.fn,
    data : data,
}


/*var selenium_credentials = {
    username: "angeltan000111+100",
    access_key: "82bed838-e26b-497c-9edb-2a68dad3ef4e"
}*/