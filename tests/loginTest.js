/*module.exports = {
	'Login Test': function(client) {
		client	
			.url("https://demo.silverstripe.org/Security/login")
			.setValue('input[name="Email"]', 'admin')
			.setValue('input[name="Password"]', 'password')
			.click('input[type="submit"]')
			.waitForElementVisible('.cms-login-status',10000)
			.end()
	}
}*/

'use strict';

module.exports = {
  '@tags': ['login'],
  
/*  before: function (client) {
    client.perform(function() {
      console.log("Start of Test Suite...");
    });
  },

  beforeEach: function(client) {
    client.perform(function() {
      console.log("Start of Test...");
    });
  },

  afterEach: function(client, done) {
    client.perform(function() {
      console.log("End of Test...");
      done();
    });
  },

  after: function(client) {
    client.perform(function() {
      console.log("End of Test Suite...");
    });
  },
*/
  'Login Test': function (client) {
    var site = client.page.loginPage();
    var data = client.globals.data.login.correct_credentials;
    site.navigate()
      .verify.title('Log in » SilverStripe Demo')
      .verify.visible('@emailField')
      .enterEmail(data.email)
      .enterPassword(data.password)
      .submitLoginForm();
    client.pause(5000);
    var loggedInView = site.section.menu;
    loggedInView.expect.element('@profileName').to.be.visible;
    client.end();
  },

 /* 'Login Test2': function (client) {
    var site = client.page.loginPage();
    site.navigate()
      .verify.title('Log in » SilverStripe Demo')
      .verify.visible('@emailField')
      .enterEmail("admin")
      .enterPassword("password")
      .submitLoginForm();
    client.pause(5000);
    var loggedInView = site.section.menu;
    loggedInView.expect.element('@profileName').to.be.visible;
    client.end();
  }*/
};