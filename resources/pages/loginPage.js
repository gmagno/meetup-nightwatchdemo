'use strict';

var url = function(){
  return this.api.launchUrl + '/Security/login';
};

var elements = {};
elements.emailField = 'input[name=Email]';
elements.passwordField = 'input[name=Password]';
elements.submitButton = 'input[type=submit]';

var sections = {};
sections.menu = { selector: '#cms-menu' };
sections.menu.elements = {};
sections.menu.elements.profileName = '.cms-login-status';

//COMMANDS
var commands = {};

commands.enterEmail = function(string) {
    this.api.pause(1000);
    return this.setValue('@emailField', string)
};
commands.enterPassword = function(string) {
    this.api.pause(1000);
    return this.setValue('@passwordField',string);
}

commands.submitLoginForm = function() {
    this.api.pause(1000);
    return this.waitForElementVisible('@submitButton', 1000)
               .click('@submitButton')
};

//EXPORT
module.exports = {
  url: url,
  elements : elements,
  sections: sections,
  commands: [commands]
};